package com.orchestronix.ediary.services;

import java.util.*;
import com.orchestronix.ediary.models.*;

public interface DiaryService {
	public void create(Diary d);
	public List<Diary> list();
	public void update(Diary d);
}
