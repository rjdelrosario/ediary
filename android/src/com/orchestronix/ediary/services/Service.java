package com.orchestronix.ediary.services;

public abstract class Service {
	public void startTrx() {
		
	}
	
	public void commitTrx() {
		
	}
	
	public void rollbackTrx() {
		
	}
}
