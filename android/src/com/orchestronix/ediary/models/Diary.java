package com.orchestronix.ediary.models;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.TableInfo;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name="Diary")
public class Diary extends Model{	
	public static final int STATUS_SAVED = 1;
	public static final int STATUS_EXPORTED = 2;
	public static final int STATUS_SENT = 3;
	public static final int EARLY_MORNING_START_HOUR = 2;
	public static final int MORNING_START_HOUR = 6;
	public static final int AFTERNOON_START_HOUR = 12;
	public static final int EVENING_START_HOUR = 18;
	public final static int INTERVAL_MINUTES = 15;
	public final static int TOTAL_MINUTES = 1440;
	
	List<DiaryEntry> emEntries = new ArrayList<DiaryEntry>();
	List<DiaryEntry> moEntries = new ArrayList<DiaryEntry>();
	List<DiaryEntry> afEntries = new ArrayList<DiaryEntry>();
	List<DiaryEntry> evEntries = new ArrayList<DiaryEntry>();
	
	@Column(name = "date")
	public String date;
	
	@Column(name = "householdNo")
	public String householdNo;
	
	@Column(name = "area")
	public String area;
	
	@Column(name = "status")
	public int status;
	
	public Diary(){}
	
	public Diary(String date, String householdNo, String area){
		super();
		this.date = date;
		this.householdNo = householdNo;
		this.area = area;
		this.status = STATUS_SAVED;
	}
	
	public static List<Diary> list(){
		return new Select()
			.from(Diary.class)
			.execute();
	}
	
	public static Diary get(String id){
		return new Select()
			.from(Diary.class)
			.where("id = ?", id)
			.executeSingle();
	}
	
	public void createEntries() {
		// Early morning
		
		LocalTime em = new LocalTime(EARLY_MORNING_START_HOUR, 0);
		LocalTime mo = new LocalTime(MORNING_START_HOUR, 0);
		LocalTime af = new LocalTime(AFTERNOON_START_HOUR, 0);
		LocalTime ev = new LocalTime(EVENING_START_HOUR, 0);

		for(int i = 0; i < Math.ceil((TOTAL_MINUTES/INTERVAL_MINUTES)*0.16)*INTERVAL_MINUTES; i += INTERVAL_MINUTES) {
			LocalTime time = em.plusMinutes(i);
			emEntries.add(DiaryEntry.createFromStart(time.getHourOfDay(), time.getMinuteOfHour(),INTERVAL_MINUTES));
		}
		
		for(int i = 0; i < Math.ceil((TOTAL_MINUTES/INTERVAL_MINUTES)*0.25)*INTERVAL_MINUTES; i += INTERVAL_MINUTES) {
			LocalTime time = mo.plusMinutes(i);
			moEntries.add(DiaryEntry.createFromStart(time.getHourOfDay(), time.getMinuteOfHour(),INTERVAL_MINUTES));
		}
		
		for(int i = 0; i < Math.ceil((TOTAL_MINUTES/INTERVAL_MINUTES)*0.25)*INTERVAL_MINUTES; i += INTERVAL_MINUTES) {
			LocalTime time = af.plusMinutes(i);
			afEntries.add(DiaryEntry.createFromStart(time.getHourOfDay(), time.getMinuteOfHour(),INTERVAL_MINUTES));
		}
		
		for(int i = 0; i < Math.ceil((TOTAL_MINUTES/INTERVAL_MINUTES)*0.33)*INTERVAL_MINUTES; i += INTERVAL_MINUTES) {
			LocalTime time = ev.plusMinutes(i);
			evEntries.add(DiaryEntry.createFromStart(time.getHourOfDay(), time.getMinuteOfHour(),INTERVAL_MINUTES));
		}
		
	}
	
	public List<DiaryEntry> getEarlyMorningEntries() {
		return new Select()
			.from(DiaryEntry.class)
			.where("diary = ? AND hour = ?", getId(), EARLY_MORNING_START_HOUR)
			.execute();
	}
	
	public List<DiaryEntry> getMorningEntries() {
		return new Select()
			.from(DiaryEntry.class)
			.where("diary = ? AND hour = ?", getId(), MORNING_START_HOUR)
			.execute();
	}
	
	public List<DiaryEntry> getAfternoonEntries() {
		return new Select()
			.from(DiaryEntry.class)
			.where("id = ? AND hour = ?", getId(), AFTERNOON_START_HOUR)
			.execute();
	}
	
	public List<DiaryEntry> getEveningEntries() {
		return new Select()
			.from(DiaryEntry.class)
			.where("id = ? AND hour = ?", getId(), EVENING_START_HOUR)
			.execute();
	}
	
	public List<DiaryEntry> getEntries() {
		return getMany(DiaryEntry.class, "diary");
	}
		
	public DiaryEntry getEntry(int hour, int minute) {
		return new Select()
			.from(DiaryEntry.class)
			.where("diary = ? AND hour = ? AND minute = ?", getId(), hour, minute)
			.executeSingle();
	}
	
	public static List<Diary> getFiltered(String area, String household, String date, int status) {
		return new Select()
			.from(Diary.class)
			.where("area LIKE ? AND householdNo LIKE ? AND status = ? AND date = ?"
					, new Object[]{area + "%", household + "%", status, date})
			.execute();
		
	}
	
	public String getImageFileName(String id) {
		return String.format("%s.jpg", id);
	}
	
	public String getCSVFileName() {
		return String.format("%s.csv", getId());
	}
	
	public void saved() {
		status = STATUS_SAVED;
	}
	
	public void exported() {
		status = STATUS_EXPORTED;
	}
	
	public void sent() {
		status = STATUS_SENT;
	}
	
	public List<DiaryEntry> getEmEntries() {
		return this.emEntries;
	}
	
	public List<DiaryEntry> getMoEntries() {
		return this.moEntries;
	}
	
	public List<DiaryEntry> getAfEntries() {
		return this.afEntries;
	}
	
	public List<DiaryEntry> getEvEntries() {
		return this.evEntries;
	}
	
	public static List<Diary> getEntries(String date) {
		return new Select()
			.from(Diary.class)
			.where("date = ?", date)
			.execute();
	}
	
	public List<String> timeEntries() {
		List<String> timeEntries = new ArrayList<String>();
		for(int i = 0; i < this.emEntries.size(); i++) {
			String start = this.emEntries.get(i).startHour + ":" + this.emEntries.get(i).startMinute;
			String end = this.emEntries.get(i).endHour + ":" + this.emEntries.get(i).endMinute;
			timeEntries.add(start + "-" + end);
		}
		return timeEntries;
	}
	
	public static List<Diary> getDiaryFromDate(String date) {
		return new Select()
			.from(Diary.class)
			.where("date = ?", date)
			.execute();
	}
	
	public static List<Diary> returnDates(String date) {
		return new Select("date")
			.from(Diary.class)
			.where("date = ?", date)
			.execute();
	}
	
	public static List<Diary> sentDiaries() {
		return new Select()
			.from(Diary.class)
			.where("status = ?", 3)
			.execute();
	}
	
	public static Diary getLastId() {
		return new Select()
			.from(Diary.class)
			.where("id = (SELECT MAX(id) FROM Diary)")
			.executeSingle();
	}
	
	public static void deleteDiaries() {
		TableInfo tableInfo = Cache.getTableInfo(Diary.class);
		ActiveAndroid.execSQL("delete from "+tableInfo.getTableName()+";");
		ActiveAndroid.execSQL("delete from sqlite_sequence where name='"+tableInfo.getTableName()+"';");
	
	}
	
	
}

