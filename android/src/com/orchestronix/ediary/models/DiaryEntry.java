package com.orchestronix.ediary.models;

import java.util.List;

import org.joda.time.LocalTime;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.TableInfo;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "DiaryEntry")
public class DiaryEntry extends Model {
	
	@Column(name = "diaryId")
	public int diaryId;
	
	@Column(name = "startHour")
	public int startHour;

	@Column(name = "startMinute")
	public int startMinute;

	@Column(name = "endHour")
	public int endHour;

	@Column(name = "endMinute")
	public int endMinute;

	@Column(name = "station")
	public String station;
	
	@Column(name = "device")
	public String device;
	
	@Column(name = "place")
	public String place;
	
	public DiaryEntry(){
		super();
	}
	

	public static DiaryEntry createFromStart(int startHour, int startMinute, int INTERVAL_MINUTES) {
		LocalTime endTime = new LocalTime(startHour, startMinute, 0).plusMinutes(INTERVAL_MINUTES);

		return new DiaryEntry()
				.withStartHour(startHour)
				.withStartMinute(startMinute)
				.withEndHour(endTime.getHourOfDay())
				.withEndMinute(endTime.getMinuteOfHour());
	}

	public DiaryEntry withStartHour(int startHour) {
		this.startHour = startHour;
		return this;
	}

	public DiaryEntry withStartMinute(int startMinute) {
		this.startMinute = startMinute;
		return this;
	}

	public DiaryEntry withEndHour(int endHour) {
		this.endHour = endHour;
		return this;
	}

	public DiaryEntry withEndMinute(int endMinute) {
		this.endMinute = endMinute;
		return this;
	}
	
	public int getStartHour() {
		return this.startHour;
	}
	
	public int getStartMinute() {
		return this.startMinute;
	}
	
	public int getEndHour() {
		return this.endHour;
	}
	
	public int getEndMinute() {
		return this.endMinute;
	}
	
	public String getStation() {
		return this.station;
	}
	
	public String getDevice() {
		return this.device;
	}
	
	public String getPlace() {
		return this.place;
	}
	
	public int getDiary() {
		return this.diaryId;
	}
	
	public void setStartHour(int startHour) {
		this.startHour = startHour;
	}
	
	public void setStartMinute(int startMinute) {
		this.startMinute = startMinute;
	}
	
	public void setEndHour(int endHour) {
		this.endHour = endHour;
	}
	
	public void setEndMinute(int endMinute) {
		this.endMinute = endMinute;
	}
	
	public void setStation(String station) {
		this.station = station;
	}
	
	public void setDevice(String device) {
		this.device = device;
	}
	
	public void setPlace(String place) {
		this.place = place;
	}
	
	public static List<DiaryEntry> list(){
		return new Select()
			.from(DiaryEntry.class)
			.execute();
	}
	
	public void setDiaryId(int diary) {
		this.diaryId = diary;
	}
	
	public static List<DiaryEntry> getAll(){
		return new Select()
		.from(DiaryEntry.class)
		.execute();
	}
	
	public static List<DiaryEntry> getEntriesInADay(String date) {
		return new Select()
			.from(DiaryEntry.class)
			.where("date = ?", date)
			.execute();
	}
	
	public static DiaryEntry get(String id){
		return new Select()
			.from(DiaryEntry.class)
			.where("id = ?", id)
			.executeSingle();
	}
	
	public static List<DiaryEntry> getFromDiary(Diary diary){
		return new Select()
			.from(DiaryEntry.class)
			.where("diary = ?", diary)
			.execute();
	}
	
	public static DiaryEntry getFromDiaryandStartTime(Diary diary, LocalTime startTime){
		return new Select()
			.from(DiaryEntry.class)
			.where("diary = ? AND startHour = ? AND startMinute = ?", diary, startTime.getHourOfDay(), startTime.getMinuteOfHour())
			//.where("startHour = ? AND startMinute = ?", startTime.getHourOfDay(), startTime.getMinuteOfHour())
			.executeSingle();
	}

	
	public static List<DiaryEntry> getStations(int from, int to){
		return new Select("station")
		.from(DiaryEntry.class)
		.where("id BETWEEN ? AND ?", from, to)
		.execute();
	}
	
	public static List<DiaryEntry> getBetween(int from, int to){
		return new Select()
			.from(DiaryEntry.class)
			.where("id BETWEEN ? AND ?", from, to)
			.execute();
	}
	
	public static List<DiaryEntry> getDiary(Diary diary) {
		return new Select()
			.from(DiaryEntry.class)
			.where("diaryId = ?", diary.getId())
			.execute();
	}
	
	public static void deleteEntries() {
		TableInfo tableInfo = Cache.getTableInfo(DiaryEntry.class);
		ActiveAndroid.execSQL("delete from "+tableInfo.getTableName()+";");
		ActiveAndroid.execSQL("delete from sqlite_sequence where name='"+tableInfo.getTableName()+"';");
	}
}
