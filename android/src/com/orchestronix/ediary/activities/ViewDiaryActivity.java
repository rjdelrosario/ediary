package com.orchestronix.ediary.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;

public class ViewDiaryActivity extends Activity{

	public static final String RESULT = "/storage/emulated/0/DiarySheets/diary.pdf";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_diary);
		 ActiveAndroid.initialize(this);
	     final String id = getIntent().getExtras().getString("id");
	     Diary diary = Diary.get(id);
	     
	     Button back = (Button) findViewById(R.id.okay);
	     TextView date = (TextView) findViewById(R.id.txtDate);
	     TextView area = (TextView) findViewById(R.id.txtArea);
	     TextView status = (TextView) findViewById(R.id.txtStatus);
	     TextView household = (TextView) findViewById(R.id.txtHousehold);
	     
	     date.setText(diary.date);
	     area.setText(diary.area);
	     switch(diary.status) {
		     case 1:	status.setText("Saved"); break;
		     case 2:	status.setText("Exported"); break;
		     case 3:	status.setText("Sent"); break;
	     }
	     household.setText(diary.householdNo);
	     
	     back.setOnClickListener( new OnClickListener() {
				public void onClick(View v){
					Intent intent = new Intent(ViewDiaryActivity.this, ViewTimelineActivity.class);
					intent.putExtra("id", id);
					startActivity(intent);
					finish();
				}
	     });
	}
	
}
