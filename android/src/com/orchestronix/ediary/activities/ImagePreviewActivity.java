package com.orchestronix.ediary.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebView;
import android.widget.ImageView;

import com.orchestronix.ediary.R;

public class ImagePreviewActivity extends Activity{
	Bitmap bmp;
	ImageView imgprev;
	WebView viewImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_diarysheet);
		
		viewImage = (WebView) findViewById(R.id.viewImage);
		viewImage.getSettings().setBuiltInZoomControls(true);
		viewImage.getSettings().setSupportZoom(true);
		viewImage.getSettings().setAllowFileAccess(true);
		viewImage.getSettings().setUseWideViewPort(true);
		viewImage.getSettings().setLoadWithOverviewMode(true);
		
		final String id = getIntent().getExtras().getString("id");
		
		String image = "file://" + Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/DiarySheets/Images/" + id + ".jpg";
		
		String generatedHtml = "<html><head></head><body>";
		
		generatedHtml = generatedHtml.concat("<img src='"+image+"' alt='Image cannot be loaded.'>");
		
		generatedHtml = generatedHtml.concat("</body></html>");
		
		viewImage.loadDataWithBaseURL("",generatedHtml, "text/html", "UTF-8", null);
	}
	
    @Override
	public void onBackPressed(){
    	Intent i = new Intent(getBaseContext(), MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
    }
	    

}
