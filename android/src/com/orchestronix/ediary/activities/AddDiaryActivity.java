package com.orchestronix.ediary.activities;


import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;

@SuppressLint("NewApi")
public class AddDiaryActivity extends Activity {
	Button btnNext;
	EditText txtArea, txtHousehold, txtPname;
	DatePicker dtDate;
	File imagesFolder;
	Intent imageIntent;
	Diary newDiaryInfo = new Diary();
	OnDateSetListener callBack;
	final Calendar mcurrentDate = Calendar.getInstance();
    int mYear = mcurrentDate.get(Calendar.YEAR);
    int mMonth = mcurrentDate.get(Calendar.MONTH);
    int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_add_diary_information);
		btnNext = (Button) findViewById(R.id.btnNext);
		dtDate = (DatePicker) findViewById(R.id.dtDate);
		txtArea = (EditText) findViewById(R.id.txtArea);
		txtHousehold = (EditText) findViewById(R.id.txtHousehold);

		dtDate.setMaxDate(System.currentTimeMillis());

		//dtDate.setMaxDate();
		btnNext.setOnClickListener( new OnClickListener() {
			public void onClick(View v){
				if(txtArea.getText().toString().trim().equals("") || txtHousehold.getText().toString().trim().equals("")){

					if(txtArea.getText().toString().trim().equals("")){
						txtArea.setError("Area is required!");
						txtArea.setHint("Enter area");
					}
					else if(txtHousehold.getText().toString().trim().equals("")){
						txtHousehold.setError("Household is required!");
						txtHousehold.setHint("Enter household");
					}
				}
				else {
					ArrayList<String> diaryInfo = new ArrayList<String>();
					newDiaryInfo.date = String.valueOf(dtDate.getMonth()+1) + "-" + String.valueOf(dtDate.getDayOfMonth() + "-"  + String.valueOf(dtDate.getYear()));
					newDiaryInfo.area = txtArea.getText().toString();
					newDiaryInfo.householdNo = txtHousehold.getText().toString();
					
					diaryInfo.add(newDiaryInfo.date);
					diaryInfo.add(newDiaryInfo.area);
					diaryInfo.add(newDiaryInfo.householdNo);
					
					//Toast.makeText(getBaseContext(), "Time " + dtDate.getMaxDate(), Toast.LENGTH_LONG).show();
					//Log.d("TAG", " " + dtDate.getMaxDate());
					Intent i = new Intent(getBaseContext(), AddTimelineActivity.class);
					i.putStringArrayListExtra("diaryInfo", diaryInfo);
					startActivity(i);
				}
			}
		});
	}
	
	


}
