package com.orchestronix.ediary.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	Button addBtn, filterBtn, sendBtn, exportBtn;

	ListView listviewDiary;
	List<Diary> info;
	List<Diary> filteredItems = new ArrayList<Diary>();
	List<String> items = new ArrayList<String>();
	ArrayList<String> filterInfo = new ArrayList<String>();
	int position;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ActiveAndroid.initialize(this);
		
		addBtn = (Button) findViewById(R.id.btnAdd);
		filterBtn = (Button) findViewById(R.id.btnFilter);
		sendBtn = (Button) findViewById(R.id.btnSend);
		exportBtn = (Button) findViewById(R.id.btnExport);
		
		listviewDiary = (ListView) findViewById(R.id.list_diary);
        info = Diary.list();
		registerForContextMenu(listviewDiary);
		
		
		addBtn.setOnClickListener( new OnClickListener() {
		
		public void onClick(View v){
				Intent addDiaryInfoIntent = new Intent(getBaseContext(), AddDiaryActivity.class);
				startActivity(addDiaryInfoIntent);
			}
		});
		
		filterBtn.setOnClickListener( new OnClickListener() {
			public void onClick(View v){
				Intent filterIntent = new Intent(getBaseContext(), FilterActivity.class);
				filterIntent.putStringArrayListExtra("filterInfo", filterInfo);
				startActivity(filterIntent);
				
			}
		});
		
		sendBtn.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SendActivity.class);
				startActivity(intent);
			}
		});
		
		exportBtn.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, ExportActivity.class);
				startActivity(intent);
				
			}
		});
		
		if(getIntent().getExtras()==null) {
			for(int i=0;i<info.size();i++)	items.add(info.get(i).toString());
		}
		else{
			filterInfo = getIntent().getStringArrayListExtra("filterInfo");
			filteredItems = Diary.getFiltered(filterInfo.get(0), filterInfo.get(1),
					filterInfo.get(2), Integer.parseInt(filterInfo.get(3)));
			for(int i = 0; i < filteredItems.size(); i++)	items.add(filteredItems.get(i).toString());
		}
	    ArrayAdapter<String> itemsAdapter = 
        	    new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        if(items.size()==0)	Toast.makeText(getBaseContext(), "No results to display", Toast.LENGTH_SHORT).show();
	    
        listviewDiary.setAdapter(itemsAdapter);
        listviewDiary.setOnItemClickListener(new OnItemClickListener()
        {
            @Override 
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            { }
        });
        
        listviewDiary.setOnItemLongClickListener(new OnItemLongClickListener() {
        	public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                    int pos, long id) {;
        		openContextMenu(listviewDiary);
        		position = pos;
                return true;
            }
        });
		
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	        ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
	      if (v.getId()==R.id.list_diary) {
	          menu.setHeaderTitle("Menu");
	          menu.add(0, v.getId(), 0, "View Diary");
	          menu.add(0, v.getId(), 0, "View Timeline");
			  menu.add(0, v.getId(), 0, "Image Preview");
			  
	     }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Diary diary = info.get(position);
		if(filteredItems.size()!=0)  diary = filteredItems.get(position);
		String idToEdit = String.valueOf(diary.getId());
        if (item.getTitle() == "View Diary") {
        	Intent editDiaryInfoIntent = new Intent(getBaseContext(), EditDiaryActivity.class);
        	ArrayList<String> filterInfoId = new ArrayList<String>();
        	if(filteredItems.size()!=0) {
        		filterInfoId.addAll(filterInfo);
        		filterInfoId.add(idToEdit);
        	}
        	else filterInfoId.add(idToEdit);
        	
        	editDiaryInfoIntent.putStringArrayListExtra("filterInfoId", filterInfoId);
        	startActivity(editDiaryInfoIntent);
        }
		else if(item.getTitle() == "View Timeline") {
        	Intent timelineIntent = new Intent(getBaseContext(), ViewTimelineActivity.class);
        	ArrayList<String> filterInfoId = new ArrayList<String>();
        	if(filteredItems.size()!=0) {
        		filterInfoId.addAll(filterInfo);
        		filterInfoId.add(idToEdit);
        	}
        	else filterInfoId.add(idToEdit);
        	
        	timelineIntent.putStringArrayListExtra("filterInfoId", filterInfoId);
        	startActivity(timelineIntent);
        }
		else if (item.getTitle() == "Image Preview") {
			Intent viewDiaryInfoIntent = new Intent(getBaseContext(), ImagePreviewActivity.class);
			viewDiaryInfoIntent.putExtra("id", idToEdit);
            startActivity(viewDiaryInfoIntent);
        }
		return false;
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.remove_filter:
				items.clear();
				for(int i=0;i<info.size();i++)	items.add(info.get(i).toString());
				ArrayAdapter<String> itemsAdapter = 
		        	    new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, items);
				listviewDiary.setAdapter(itemsAdapter);
				filterInfo.clear();
				filteredItems.clear();
				return true;
			case R.id.remove_all:
				
				Diary.deleteDiaries();
				DiaryEntry.deleteEntries();
				File dir = new File(Environment.getExternalStorageDirectory() + "/DiarySheets");
				File exports = new File(dir.getPath() + "/Exports");
				File images = new File(dir.getPath() + "/Images");
				File compressed = new File(dir.getPath() + "/Compressed");
				
				deleteContents(exports);
				deleteContents(images);
				deleteContents(compressed);
				deleteContents(dir);
				
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public void deleteContents(File dir) {
		if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            new File(dir, children[i]).delete();
	        }
	    }
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	
}
