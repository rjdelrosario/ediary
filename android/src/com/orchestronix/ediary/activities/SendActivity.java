package com.orchestronix.ediary.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.Mail;

@SuppressLint("NewApi")
public class SendActivity extends Activity{
	ListView lvExports;
	String date;
	List<Diary> entries;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send);
		lvExports = (ListView) findViewById(R.id.lvExports);
		File diaryFiles = new File(Environment.getExternalStorageDirectory(), "DiarySheets");
		File storage = new File(diaryFiles.getPath() + "/Exports");
		final ArrayList<String> FilesInFolder = GetFiles(storage.getPath());
		List<Diary> diaryList = Diary.sentDiaries();
		
        if(FilesInFolder!=null) {
        	for(int i = 0; i < diaryList.size(); i++)	FilesInFolder.remove(diaryList.get(i).date + ".csv");
        	lvExports.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FilesInFolder));
        	if(FilesInFolder.size()==0)	Toast.makeText(getBaseContext(), "No files available for sending", Toast.LENGTH_SHORT).show();
        }
        else Toast.makeText(getBaseContext(), "No files available for sending", Toast.LENGTH_SHORT).show();
        
		lvExports.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		            date = FilesInFolder.get(position);
		            date = date.substring(0, date.indexOf("."));
		            entries = Diary.getEntries(date);
		            AlertDialog.Builder confirmSend = new AlertDialog.Builder(SendActivity.this);
		            confirmSend.setTitle("Confirm sending..");
		            confirmSend.setMessage("Send exports for this date?: " + date);
		            confirmSend.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								sendEmail(date);
							}
					});
		            confirmSend.setNegativeButton("No", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
					});
		            confirmSend.show();
		            
		    }
		});

	}

	public void sendEmail(String toSend) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
				final Mail m = new Mail("sampletrialemail@gmail.com", "sampletrial"); 
				
				String[] toArr = {"jbanares@orchestronix.com"}; 
				m.setTo(toArr); 
				m.setFrom("rmangilit@orchestronix.com"); 
				m.setSubject("Test E-Mail"); 
				m.setBody("Email body."); 
				
				try {
					//for attachments
					byte[] buffer = new byte[1024];
					
					try{
						File home = new File(Environment.getExternalStorageDirectory(), "DiarySheets");
						File images = new File(home.getPath() + "/Images");
						File csv = new File(home.getPath() + "/Exports");
						File zipStorage = new File(home.getPath() + "/Compressed");
		            	if(!zipStorage.exists()) zipStorage.mkdirs();
						
						
						FileOutputStream fos = new FileOutputStream(zipStorage.getPath()+"/"+this.date+"-export.zip");
						ZipOutputStream zos = new ZipOutputStream(fos);
						
						List<Diary> de = Diary.getDiaryFromDate(toSend);
						
						// add files
						// fileList -> List of files to be included in the zip
						for(int i=0; i<de.size(); i++){
							String fileName = de.get(i).getId()+".jpg";
							
							ZipEntry ze= new ZipEntry(fileName);
							zos.putNextEntry(ze);
							
							FileInputStream in = new FileInputStream(images.getPath() + File.separator + fileName);
							
							int len;
							while ((len = in.read(buffer)) > 0) {
								zos.write(buffer, 0, len);
							}
						
							in.close();
						}
						
						ZipEntry ze= new ZipEntry(this.date+ ".csv");
						zos.putNextEntry(ze);
						
						FileInputStream in = new FileInputStream(csv.getPath() + File.separator + this.date+ ".csv");
						
						int len;
						while ((len = in.read(buffer)) > 0) {
							zos.write(buffer, 0, len);
						}
					
						in.close();
						
						zos.closeEntry();
						//remember close it
						zos.close();
						
					}catch(IOException ex){
						ex.printStackTrace();   
					}
					
					File storage = new File(Environment.getExternalStorageDirectory(), "DiarySheets");
					String file = storage.getAbsolutePath().toString() + "/Compressed/"+this.date+"-export.zip";
					File attachment = new File(file);
					if(attachment.exists())	m.addAttachment(file); 
					else Toast.makeText(SendActivity.this, "No such file!", Toast.LENGTH_SHORT).show();
					new Thread(
							new Runnable(){
								@Override
								public void run() {
									Looper.prepare();
									try {
										if(m.send()) {
											for(int i = 0; i < entries.size(); i++) {
								            	entries.get(i).sent();
								            	entries.get(i).save();
								            }
											NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
											CharSequence tickerText = "Email was successfully sent"; // ticker-text
											Intent notificationIntent = new Intent(getBaseContext(), MainActivity.class);
											PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, notificationIntent, 0);
											
											Notification notification = new Notification(R.drawable.ic_action_email, tickerText, System.currentTimeMillis());
											notification.setLatestEventInfo(
													getApplicationContext(),
													"Email was successfully sent",
													"",
													contentIntent
													);
											// and this
											final int HELLO_ID = 1;
											mNotificationManager.notify(HELLO_ID, notification);
										}
										else Toast.makeText(SendActivity.this, "Email was not sent.", Toast.LENGTH_LONG).show();
										
									} catch (Exception e){ e.printStackTrace();}

									

								}
							}
					).start();

				}
				catch(Exception e) { 
					Toast.makeText(SendActivity.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show(); 
				}
				Intent returnIntent = new Intent(SendActivity.this, MainActivity.class);
				returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(returnIntent);

	}
	
	public ArrayList<String> GetFiles(String DirectoryPath) {
	    ArrayList<String> MyFiles = new ArrayList<String>();
	    File f = new File(DirectoryPath);

	    f.mkdirs();
	    File[] files = f.listFiles();
	    if (files.length == 0)
	        return null;
	    else {
	        for (int i=0; i<files.length; i++) 
	            MyFiles.add(files[i].getName());
	    }

	    return MyFiles;
	}
}
