package com.orchestronix.ediary.activities;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("NewApi")
public class EditTimelineActivity extends FragmentActivity implements ActionBar.TabListener {

	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	ArrayList<String> filterInfoId;
	ArrayList<String> filterInfo = new ArrayList<String>();
	EarlyMorningTab earlyMorningTab = null;
	MorningTab morningTab = null;
	AfternoonTab afternoonTab = null;
	EveningTab eveningTab = null;
	Diary diary;
	int id;
	int passedId;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_timeline);
        
        ActiveAndroid.initialize(this);
        
        Intent intent = getIntent();
		filterInfoId = intent.getStringArrayListExtra("filterInfoId");
		if(filterInfoId.size()!=1) {
			for(int i = 0; i < filterInfoId.size()-1; i++)	filterInfo.add(filterInfoId.get(i));
			passedId = Integer.parseInt(filterInfoId.get(filterInfoId.size()-1));
			this.diary = Diary.get(filterInfoId.get(filterInfoId.size()-1)); 
		}
		else {
			passedId = Integer.parseInt(filterInfoId.get(0));
			diary = Diary.get(filterInfoId.get(filterInfoId.size()-1));
		}
		
        
		 // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // For each of the sections in the app, add a tab to the action bar.
        actionBar.addTab(actionBar.newTab().setText("Early Morning").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Morning").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Afternoon").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Evening").setTabListener(this));
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_timeline, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.edit_action_accept:
            	/*
            	 * TODO
            	 * 	- improve checking
            	 * */
            	
            	
            	List<String> checkStation = new ArrayList<String>();
            	List<String> checkPlaces = new ArrayList<String>();
            	List<String> checkDevices = new ArrayList<String>();
            	
            	if(this.earlyMorningTab != null){
            		checkStation.addAll(this.earlyMorningTab.getStations());
            		checkPlaces.addAll(this.earlyMorningTab.getPlaces());
            		checkDevices.addAll(this.earlyMorningTab.getDevices());
            	}
    			if(this.morningTab != null){
    				checkStation.addAll(this.morningTab.getStations());
    				checkPlaces.addAll(this.morningTab.getPlaces());
    				checkDevices.addAll(this.morningTab.getDevices());
    			}
    			if(this.afternoonTab != null){
    				checkStation.addAll(this.afternoonTab.getStations());
    				checkPlaces.addAll(this.afternoonTab.getPlaces());
    				checkDevices.addAll(this.afternoonTab.getDevices());
    			}
    			if(this.eveningTab != null){
    				checkStation.addAll(this.eveningTab.getStations());
    				checkPlaces.addAll(this.eveningTab.getPlaces());
    				checkDevices.addAll(this.eveningTab.getDevices());
    			}
    			
    			ArrayList<String> remove = new ArrayList<String>();
    			remove.add("-");
    			
    			checkStation.removeAll(remove);
    			checkPlaces.removeAll(remove);
    			checkDevices.removeAll(remove);
            	
    			if(checkStation.size() != 0 && checkPlaces.size() != 0 && checkDevices.size() != 0 && checkStation.size() == checkPlaces.size() && checkPlaces.size() == checkDevices.size()) {
	            	onSaveAction();
	            	Intent saveIntent = new Intent(EditTimelineActivity.this,MainActivity.class);
	            	if(filterInfo.size()!=0)	saveIntent.putStringArrayListExtra("filterInfo", filterInfo);
	            	saveIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(saveIntent);
    			}
    			else Toast.makeText(getBaseContext(), "Please fill out all necessary fields.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        
    	switch(tab.getPosition()){
			case 0:
				if(this.earlyMorningTab != null){
					List<String> stations = this.earlyMorningTab.getStations();
					List<String> devices = this.earlyMorningTab.getDevices();
					List<String> places = this.earlyMorningTab.getPlaces();
					
					this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
					
					List<ListTableRow> rows = this.earlyMorningTab.getRows();
					
					for(int i=0; i<this.earlyMorningTab.getSize(); i++){
						if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
				else{
					this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
					
					List<ListTableRow> rows = this.earlyMorningTab.getRows();
					this.id = (passedId-1)*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES);
					//this.id = id-1*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES);
					
					for(int i=0; i<this.earlyMorningTab.getSize(); i++){
						this.id = this.id+1;
						DiaryEntry diaryEntry = DiaryEntry.get(Integer.toString(this.id));
						
						if(diaryEntry.getStation().compareTo("-") == 0) continue;
						
						rows.get(i).setStation(diaryEntry.getStation());
						rows.get(i).setDevice(diaryEntry.getDevice());
						rows.get(i).setPlace(diaryEntry.getPlace());
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
	    		getSupportFragmentManager().beginTransaction().replace(R.id.editContainer, this.earlyMorningTab).commit();
				break;
			case 1:
				if(this.morningTab != null){
					List<String> stations = this.morningTab.getStations();
					List<String> devices = this.morningTab.getDevices();
					List<String> places = this.morningTab.getPlaces();
					
					this.morningTab = new MorningTab(getBaseContext());
					
					List<ListTableRow> rows = this.morningTab.getRows();
					
					for(int i=0; i<this.morningTab.getSize(); i++){						
						if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
				else{
					this.morningTab = new MorningTab(getBaseContext());
					
					List<ListTableRow> rows = this.morningTab.getRows();
					
					this.id++;
					
					for(int i=0; i<this.morningTab.getSize(); i++){
						DiaryEntry diaryEntry = DiaryEntry.get(Integer.toString(this.id++));
						
						if(diaryEntry.getStation().compareTo("-") == 0) continue;
						
						rows.get(i).setStation(diaryEntry.getStation());
						rows.get(i).setDevice(diaryEntry.getDevice());
						rows.get(i).setPlace(diaryEntry.getPlace());
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
	    		getSupportFragmentManager().beginTransaction().replace(R.id.editContainer, this.morningTab).commit();
				break;
			case 2:
				if(this.afternoonTab != null){
					List<String> stations = this.afternoonTab.getStations();
					List<String> devices = this.afternoonTab.getDevices();
					List<String> places = this.afternoonTab.getPlaces();
					
					this.afternoonTab = new AfternoonTab(getBaseContext());
					
					List<ListTableRow> rows = this.afternoonTab.getRows();
					
					for(int i=0; i<this.afternoonTab.getSize(); i++){
						if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
				else{
					this.afternoonTab = new AfternoonTab(getBaseContext());
					
					List<ListTableRow> rows = this.afternoonTab.getRows();
					
					for(int i=0; i<this.afternoonTab.getSize(); i++){
						DiaryEntry diaryEntry = DiaryEntry.get(Integer.toString(this.id++));
						
						if(diaryEntry.getStation().compareTo("-") == 0) continue;
						
						rows.get(i).setStation(diaryEntry.getStation());
						rows.get(i).setDevice(diaryEntry.getDevice());
						rows.get(i).setPlace(diaryEntry.getPlace());
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
	    		getSupportFragmentManager().beginTransaction().replace(R.id.editContainer, this.afternoonTab).commit();
				break;
			case 3:
				if(this.eveningTab != null){
					List<String> stations = this.eveningTab.getStations();
					List<String> devices = this.eveningTab.getDevices();
					List<String> places = this.eveningTab.getPlaces();
					
					this.eveningTab = new EveningTab(getBaseContext());
					
					List<ListTableRow> rows = this.eveningTab.getRows();
					
					for(int i=0; i<this.eveningTab.getSize(); i++){
						if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
				else{
					this.eveningTab = new EveningTab(getBaseContext());
					
					List<ListTableRow> rows = this.eveningTab.getRows();
					
					for(int i=0; i<this.eveningTab.getSize(); i++){
						DiaryEntry diaryEntry = DiaryEntry.get(Integer.toString(this.id++));
						
						if(diaryEntry.getStation().compareTo("-") == 0) continue;
						
						rows.get(i).setStation(diaryEntry.getStation());
						rows.get(i).setDevice(diaryEntry.getDevice());
						rows.get(i).setPlace(diaryEntry.getPlace());
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
					}
				}
	    		getSupportFragmentManager().beginTransaction().replace(R.id.editContainer, this.eveningTab).commit();
				break;
		}
    	
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public static class DummySectionFragment extends Fragment {
        public DummySectionFragment() {
        }

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            TextView textView = new TextView(getActivity());
            textView.setGravity(Gravity.CENTER);
            Bundle args = getArguments();
            textView.setText(Integer.toString(args.getInt(ARG_SECTION_NUMBER)));
            return textView;
        }
    }
    
    public void onSaveAction() {
		/*this.diary.date = this.filterInfo.get(0);
		this.diary.area = this.filterInfo.get(1);
		this.diary.householdNo = this.filterInfo.get(2);
		this.diary.save();*/
		
		if(this.earlyMorningTab == null) this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
		if(this.morningTab == null) this.morningTab = new MorningTab(getBaseContext());
		if(this.afternoonTab == null) this.afternoonTab = new AfternoonTab(getBaseContext());
		if(this.eveningTab == null) this.eveningTab = new EveningTab(getBaseContext());
		
		ArrayList<LocalTime> startTime = new ArrayList<LocalTime>();
		ArrayList<String> stations = new ArrayList<String>();
		ArrayList<String> devices = new ArrayList<String>();
		ArrayList<String> places = new ArrayList<String>();
		
		startTime.addAll(this.earlyMorningTab.getStartTimeSegments());
		startTime.addAll(this.morningTab.getStartTimeSegments());
		startTime.addAll(this.afternoonTab.getStartTimeSegments());
		startTime.addAll(this.eveningTab.getStartTimeSegments());
		
		stations.addAll(this.earlyMorningTab.getStations());
		stations.addAll(this.morningTab.getStations());
		stations.addAll(this.afternoonTab.getStations());
		stations.addAll(this.eveningTab.getStations());
		
		devices.addAll(this.earlyMorningTab.getDevices());
		devices.addAll(this.morningTab.getDevices());
		devices.addAll(this.afternoonTab.getDevices());
		devices.addAll(this.eveningTab.getDevices());
		
		places.addAll(this.earlyMorningTab.getPlaces());
		places.addAll(this.morningTab.getPlaces());
		places.addAll(this.afternoonTab.getPlaces());
		places.addAll(this.eveningTab.getPlaces());
		
		int totalSize = this.earlyMorningTab.getSize() + this.morningTab.getSize() + this.afternoonTab.getSize() + this.eveningTab.getSize();
		int eid = (passedId-1)*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES);
		//int id = this.id-1*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES);
		
		for(int i = 0; i< totalSize; i++){
			DiaryEntry diaryEntry = DiaryEntry.get(Integer.toString(eid+i+1));
			diaryEntry.setStation(stations.get(i));
			diaryEntry.setDevice(devices.get(i));
			diaryEntry.setPlace(places.get(i));
			diaryEntry.setDiaryId(diary.getId().intValue());
			diaryEntry.save();
		}
		
		//Toast.makeText(getBaseContext(), "Timeline successfully edited!" + filterInfo.size(), Toast.LENGTH_SHORT).show();
		
		//startActivity(intent);
	}

}
