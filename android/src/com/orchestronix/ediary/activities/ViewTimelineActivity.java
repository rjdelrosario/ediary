package com.orchestronix.ediary.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalTime;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("NewApi")
public class ViewTimelineActivity extends Activity {
	ArrayList<String> filterInfoId;
	ArrayList<String> filterInfo = new ArrayList<String>();
	String id;
	WebView viewTimeline;
	View loadingScreen;
	int mShortAnimationDuration;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_timeline);
		
		viewTimeline = (WebView) findViewById(R.id.viewTimeline);
		loadingScreen = (View) findViewById(R.id.loading_spinner);
		
		viewTimeline.getSettings().setBuiltInZoomControls(true);
		viewTimeline.getSettings().setSupportZoom(true);
		
		viewTimeline.setVisibility(View.GONE);
		
		mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
		
		crossfade();
		
		Intent intent = getIntent();
		filterInfoId = intent.getStringArrayListExtra("filterInfoId");
		
		if(filterInfoId.size()!=1) {
			for(int i = 0; i < filterInfoId.size()-1; i++)	filterInfo.add(filterInfoId.get(i));
			id = filterInfoId.get(filterInfoId.size()-1);
		}
		else id = filterInfoId.get(0);
		
		int startRange = (Integer.parseInt(id)-1)*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES) + 1;
		int endRange = Integer.parseInt(id)*(Diary.TOTAL_MINUTES/Diary.INTERVAL_MINUTES);
		
		List<DiaryEntry> entries = DiaryEntry.getBetween(startRange, endRange);
		List<String> stations = new ArrayList<String>();
		Map<String, Integer> placesMap = new HashMap<String, Integer>();
		placesMap.put("1 - Home", 1);
		placesMap.put("2 - Work", 2);
		placesMap.put("3 - Private Vehicle", 3);
		placesMap.put("4 - Bus", 4);
		placesMap.put("5 - Van/FX", 5);
		placesMap.put("6 - Jeepney", 6);
		placesMap.put("7 - Taxi", 7);
		placesMap.put("8 - Other Vehicles", 8);
		placesMap.put("9 - Internet Cafe", 9);
		placesMap.put("0 - Elsewhere", 0);
		
		Map<String, String> deviceMap = new HashMap<String, String>();
        deviceMap.put("A - Radio Cassette/Recorder","A");
        deviceMap.put("B - Stereo/Component","B");
		deviceMap.put("C - Karaoke","C");
		deviceMap.put("D - DVD/VCD Players","D");
		deviceMap.put("E - Car Radio","E");
		deviceMap.put("F - Transistor Radio","F");
		deviceMap.put("-","-");
		
		for(int i=0; i<entries.size(); i++){
			String currStation = entries.get(i).getStation();
			if(i == 0) stations.add(currStation);
			else{
				int j;
				for(j=0; j<stations.size(); j++) if(currStation.compareTo(stations.get(j))==0) break;
				if(j==stations.size()) stations.add(currStation);
			}
		}
		
		stations.remove("-");
		
		String generatedHtml = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" /></head>";
		
		generatedHtml = generatedHtml.concat("<body><table>");
		
		generatedHtml = generatedHtml.concat("<tr>");
			generatedHtml = generatedHtml.concat("<td rowspan='2' colspan='2'>TIME</td>");
			for(int i=0; i<stations.size(); i++) generatedHtml = generatedHtml.concat("<td rowspan='2' class='cell_padding stations'><p class='labels'>"+stations.get(i)+"</p></td>");
			for(int i=stations.size(); i<8; i++) generatedHtml = generatedHtml.concat("<td rowspan='2' class='cell_padding stations'><p class='labels'>-</p></td>");
			generatedHtml = generatedHtml.concat("<td colspan='11'>Place of Listening</td>");
		generatedHtml = generatedHtml.concat("</tr>");
		
		generatedHtml = generatedHtml.concat("<tr>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Home</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Work</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Private Vehicle</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Bus</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Van/FX</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Jeepney</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Taxi</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Other Vehicles</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Internet Cafe</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>Elsewhere</p></td>");
			generatedHtml = generatedHtml.concat("<td class='cell_padding places_devices'><p class='labels'>DEVICE</p></td>");
		generatedHtml = generatedHtml.concat("</tr>");
		
		for(int i=0; i< entries.size(); i++){
			DiaryEntry entry = entries.get(i);
			
			LocalTime startTime = new LocalTime(entry.getStartHour(), entry.getStartMinute(), 0);
			LocalTime endTime = new LocalTime(entry.getEndHour(), entry.getEndMinute(), 0);
			
			generatedHtml = generatedHtml.concat("<tr>");
				generatedHtml = generatedHtml.concat("<td>"+(i+1)+"</td>");
				generatedHtml = generatedHtml.concat("<td class='time_intervals'>"+startTime.toString("h:mm")+" - "+endTime.toString("h:mm a")+"</td>");
				
				if(entry.getStation().compareTo("-")==0){
					for(int j=0; j<8; j++) generatedHtml = generatedHtml.concat("<td>"+(j+1)+"</td>");
					for(int j=0; j<9; j++) generatedHtml = generatedHtml.concat("<td>"+(j+1)+"</td>");
					generatedHtml = generatedHtml.concat("<td>0</td>");
					generatedHtml = generatedHtml.concat("<td>-</td>");
				}
				else{
					for(int j=0; j<stations.size(); j++){
						if(stations.get(j).compareTo(entry.getStation())==0) generatedHtml = generatedHtml.concat("<td class='selected'>"+(j+1)+"</td>");
						else generatedHtml = generatedHtml.concat("<td>"+(j+1)+"</td>");
					}
					for(int j=stations.size(); j<8; j++) generatedHtml = generatedHtml.concat("<td>"+(j+1)+"</td>");
					
					int selected = placesMap.get(entry.getPlace());
					
					for(int j=0; j<10; j++){
						if(((j+1)%10)==selected) generatedHtml = generatedHtml.concat("<td class='selected'>"+selected+"</td>");
						else generatedHtml = generatedHtml.concat("<td>"+((j+1)%10)+"</td>");
					}
					
					generatedHtml = generatedHtml.concat("<td>"+deviceMap.get(entry.getDevice())+"</td>");
					
				}
			generatedHtml = generatedHtml.concat("</tr>");

		}
		
		generatedHtml = generatedHtml.concat("</table></body></html>");
		
		viewTimeline.loadDataWithBaseURL("file:///android_asset/", generatedHtml, "text/html", "UTF-8", null);
		
	}
	
		@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu items for use in the action bar
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.edit, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle presses on the action bar items
	        switch (item.getItemId()) {
	            case R.id.add_action_edit:
	            	Diary diary = Diary.get(id);
	            	
	            	if(diary.status == 2) {
	            		Toast.makeText(getBaseContext(), "Exported entries cannot be edited!", Toast.LENGTH_SHORT).show();
	            	}
	            	else if(diary.status == 3) {
	            		Toast.makeText(getBaseContext(), "Sent entries cannot be edited!", Toast.LENGTH_SHORT).show();
	            	}
	            	else {
		            	ArrayList<String> id = new ArrayList<String>();
		            	id.add(this.id);
		            	Intent i = new Intent(getBaseContext(), EditTimelineActivity.class);
		            	if(filterInfoId.size()!=1)	i.putStringArrayListExtra("filterInfoId", filterInfoId);
		            	else i.putStringArrayListExtra("filterInfoId", id);
		                startActivity(i);
	            	}
	        }
	        return true;
	    }
	    
	    private void crossfade() {

	        viewTimeline.setAlpha(0f);
	        viewTimeline.setVisibility(View.VISIBLE);

	        viewTimeline.animate()
	                .alpha(1f)
	                .setDuration(mShortAnimationDuration)
	                .setListener(null);

	        
	        loadingScreen.animate()
	                .alpha(0f)
	                .setDuration(mShortAnimationDuration)
	                .setListener(new AnimatorListenerAdapter() {
	                    @Override
	                    public void onAnimationEnd(Animator animation) {
	                        loadingScreen.setVisibility(View.GONE);
	                    }
	                });
	    }
	
}
