package com.orchestronix.ediary.activities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.orchestronix.ediary.R;

@SuppressLint("NewApi")
public class FilterActivity extends Activity{
	EditText area, household;
	DatePicker filterDate;
	Spinner status;
	Button filter, clear;
	ArrayList<String> filterInfo;
	ArrayAdapter<CharSequence> dataAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filter);
		
		filterInfo = getIntent().getStringArrayListExtra("filterInfo");
		
		area = (EditText) findViewById (R.id.txtFilterArea);
		household = (EditText) findViewById (R.id.txtFilterHousehold);
		filterDate = (DatePicker) findViewById(R.id.dtFilterDate);
		status = (Spinner) findViewById(R.id.spStatus);
		filter = (Button) findViewById(R.id.btnAddFilter);
		clear = (Button) findViewById(R.id.btnClearFilter);
		
		filterDate.setMaxDate(System.currentTimeMillis());
		
		dataAdapter = ArrayAdapter.createFromResource(getApplicationContext(),
				R.array.statusArray, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        status.setAdapter(dataAdapter);
		if(filterInfo.size()!=0){
			area.setText(filterInfo.get(0));
			household.setText(filterInfo.get(1));
			String[] dates = new String[3];
			dates = filterInfo.get(2).split("-");
			filterDate.updateDate(Integer.parseInt(dates[2]), Integer.parseInt(dates[0])-1, Integer.parseInt(dates[1]));
			switch(Integer.parseInt(filterInfo.get(3))) {
		     case 1:	status.setSelection(0); break;
		     case 2:	status.setSelection(1); break;
		     case 3:	status.setSelection(2); break;
	    	}
		}
		
		

        filter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				filterInfo = new ArrayList<String>();
				filterInfo.add(area.getText().toString());
				filterInfo.add(household.getText().toString());
				filterInfo.add(String.valueOf(filterDate.getMonth()+1) + "-" + String.valueOf(filterDate.getDayOfMonth()
						+ "-" + String.valueOf(filterDate.getYear())));
				if(status.getSelectedItem().toString().compareTo("Saved")==0)	filterInfo.add("1");
				else if(status.getSelectedItem().toString().compareTo("Exported")==0)	filterInfo.add("2");
				else if(status.getSelectedItem().toString().compareTo("Sent")==0)	filterInfo.add("3"); 
				Intent filterIntent = new Intent(FilterActivity.this, MainActivity.class);
				filterIntent.putStringArrayListExtra("filterInfo", filterInfo);
				startActivity(filterIntent);
				finish();
			}
		});
        
        clear.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				area.setText("");
				household.setText("");
			}
		});
		
		
	}
}