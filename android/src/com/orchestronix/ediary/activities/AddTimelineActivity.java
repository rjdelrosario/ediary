package com.orchestronix.ediary.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("NewApi")
public class AddTimelineActivity extends FragmentActivity implements ActionBar.TabListener {  
	
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	ArrayList<String> diaryInfo;
	EarlyMorningTab earlyMorningTab = null;
	MorningTab morningTab = null;
	AfternoonTab afternoonTab = null;
	EveningTab eveningTab = null;
	String imageFilePath;
	Uri imageFileUri;
	File storage, originalFile, imgFolder;
	Diary diary;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
		setContentView(R.layout.activity_add_timeline);
        
		Intent intent;
		intent = getIntent();
        diaryInfo = intent.getStringArrayListExtra("diaryInfo");

		 // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // For each of the sections in the app, add a tab to the action bar.
        actionBar.addTab(actionBar.newTab().setText("Early Morning").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Morning").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Afternoon").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Evening").setTabListener(this));
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_timeline, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.add_action_camera:
            	/*
            	 * TODO
            	 * 	- improve checking
            	 * */
        		
            	
            	List<String> checkStation = new ArrayList<String>();
            	List<String> checkPlaces = new ArrayList<String>();
            	List<String> checkDevices = new ArrayList<String>();
            	
            	if(this.earlyMorningTab != null){
            		checkStation.addAll(this.earlyMorningTab.getStations());
            		checkPlaces.addAll(this.earlyMorningTab.getPlaces());
            		checkDevices.addAll(this.earlyMorningTab.getDevices());
            	}
    			if(this.morningTab != null){
    				checkStation.addAll(this.morningTab.getStations());
    				checkPlaces.addAll(this.morningTab.getPlaces());
    				checkDevices.addAll(this.morningTab.getDevices());
    			}
    			if(this.afternoonTab != null){
    				checkStation.addAll(this.afternoonTab.getStations());
    				checkPlaces.addAll(this.afternoonTab.getPlaces());
    				checkDevices.addAll(this.afternoonTab.getDevices());
    			}
    			if(this.eveningTab != null){
    				checkStation.addAll(this.eveningTab.getStations());
    				checkPlaces.addAll(this.eveningTab.getPlaces());
    				checkDevices.addAll(this.eveningTab.getDevices());
    			}
    			
    			ArrayList<String> remove = new ArrayList<String>();
    			remove.add("-");
    			
    			checkStation.removeAll(remove);
    			checkPlaces.removeAll(remove);
    			checkDevices.removeAll(remove);
    			
    			if(checkStation.size() != 0 && checkPlaces.size() != 0 && checkDevices.size() != 0 && checkStation.size() == checkPlaces.size() && checkPlaces.size() == checkDevices.size()) {
	            	storage = new File(Environment.getExternalStorageDirectory() + "/DiarySheets");
	            	if(!storage.exists()) storage.mkdirs();
	            	diary = new Diary(this.diaryInfo.get(0), this.diaryInfo.get(2), this.diaryInfo.get(1));
	            	String imageName;
	            	if((Diary.getLastId()==null)) imageName = "1";
	            	else imageName = String.valueOf((Diary.getLastId().getId().intValue()+1));
	            	
	            	imgFolder = new File(storage.getPath()+"/Images");
	            	if(!imgFolder.exists()) imgFolder.mkdirs();
	            	
	            	imageFilePath = imgFolder + File.separator + imageName + ".jpg";
	            	originalFile = new File(imageFilePath);
	            	imageFileUri = Uri.fromFile(originalFile);
	            	
	            	Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					imageIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
	            	startActivityForResult(imageIntent, 0);
    			}
    			else Toast.makeText(getBaseContext(), "Please fill out all necessary fields.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        
    	switch(tab.getPosition()){
    		case 0:
    			if(this.earlyMorningTab != null){
    				List<String> stations = this.earlyMorningTab.getStations();
    				List<String> devices = this.earlyMorningTab.getDevices();
    				List<String> places = this.earlyMorningTab.getPlaces();
    				
    				this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
    				
    				List<ListTableRow> rows = this.earlyMorningTab.getRows();
    				
    				for(int i=0; i<this.earlyMorningTab.getSize(); i++){
    					if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
    				}
    			}
    			else this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
        		getSupportFragmentManager().beginTransaction().replace(R.id.addContainer, this.earlyMorningTab).commit();
    			break;
    		case 1:
    			if(this.morningTab != null){
    				List<String> stations = this.morningTab.getStations();
    				List<String> devices = this.morningTab.getDevices();
    				List<String> places = this.morningTab.getPlaces();
    				
    				this.morningTab = new MorningTab(getBaseContext());
    				
    				List<ListTableRow> rows = this.morningTab.getRows();
    				
    				for(int i=0; i<this.morningTab.getSize(); i++){
    					if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
    				}
    			}
    			else this.morningTab = new MorningTab(getBaseContext());
        		getSupportFragmentManager().beginTransaction().replace(R.id.addContainer, this.morningTab).commit();
    			break;
    		case 2:
    			if(this.afternoonTab != null){
    				List<String> stations = this.afternoonTab.getStations();
    				List<String> devices = this.afternoonTab.getDevices();
    				List<String> places = this.afternoonTab.getPlaces();
    				
    				this.afternoonTab = new AfternoonTab(getBaseContext());
    				
    				List<ListTableRow> rows = this.afternoonTab.getRows();
    				
    				for(int i=0; i<this.afternoonTab.getSize(); i++){
    					if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
    				}
    			}
    			else this.afternoonTab = new AfternoonTab(getBaseContext());
        		getSupportFragmentManager().beginTransaction().replace(R.id.addContainer, this.afternoonTab).commit();
    			break;
    		case 3:
    			if(this.eveningTab != null){
    				List<String> stations = this.eveningTab.getStations();
    				List<String> devices = this.eveningTab.getDevices();
    				List<String> places = this.eveningTab.getPlaces();
    				
    				this.eveningTab = new EveningTab(getBaseContext());
    				
    				List<ListTableRow> rows = this.eveningTab.getRows();
    				
    				for(int i=0; i<this.eveningTab.getSize(); i++){
    					if(stations.get(i).compareTo("-") == 0) continue;
						
						rows.get(i).setStation(stations.get(i));
						rows.get(i).setDevice(devices.get(i));
						rows.get(i).setPlace(places.get(i));
						
						rows.get(i).getStationSpinner().setEnabled(true);
						rows.get(i).getDeviceSpinner().setEnabled(true);
						rows.get(i).getPlaceSpinner().setEnabled(true);
    				}
    			}
    			else this.eveningTab = new EveningTab(getBaseContext());
        		getSupportFragmentManager().beginTransaction().replace(R.id.addContainer, this.eveningTab).commit();
    			break;
    	}
    	
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public static class DummySectionFragment extends Fragment {
        public DummySectionFragment() {
        }

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            TextView textView = new TextView(getActivity());
            textView.setGravity(Gravity.CENTER);
            Bundle args = getArguments();
            textView.setText(Integer.toString(args.getInt(ARG_SECTION_NUMBER)));
            return textView;
        }
    }
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK && requestCode == 0 && resultCode != RESULT_CANCELED) {
			diary.save();
			
			if(this.earlyMorningTab == null) this.earlyMorningTab = new EarlyMorningTab(getBaseContext());
			if(this.morningTab == null) this.morningTab = new MorningTab(getBaseContext());
			if(this.afternoonTab == null) this.afternoonTab = new AfternoonTab(getBaseContext());
			if(this.eveningTab == null) this.eveningTab = new EveningTab(getBaseContext());
			
			ArrayList<LocalTime> startTime = new ArrayList<LocalTime>();
			ArrayList<String> stations = new ArrayList<String>();
			ArrayList<String> devices = new ArrayList<String>();
			ArrayList<String> places = new ArrayList<String>();
			
			startTime.addAll(this.earlyMorningTab.getStartTimeSegments());
			startTime.addAll(this.morningTab.getStartTimeSegments());
			startTime.addAll(this.afternoonTab.getStartTimeSegments());
			startTime.addAll(this.eveningTab.getStartTimeSegments());
			
			stations.addAll(this.earlyMorningTab.getStations());
			stations.addAll(this.morningTab.getStations());
			stations.addAll(this.afternoonTab.getStations());
			stations.addAll(this.eveningTab.getStations());
			
			devices.addAll(this.earlyMorningTab.getDevices());
			devices.addAll(this.morningTab.getDevices());
			devices.addAll(this.afternoonTab.getDevices());
			devices.addAll(this.eveningTab.getDevices());
			
			places.addAll(this.earlyMorningTab.getPlaces());
			places.addAll(this.morningTab.getPlaces());
			places.addAll(this.afternoonTab.getPlaces());
			places.addAll(this.eveningTab.getPlaces());
			
			int totalSize = this.earlyMorningTab.getSize() + this.morningTab.getSize() + this.afternoonTab.getSize() + this.eveningTab.getSize();
			
			for(int i = 0; i< totalSize; i++){
				DiaryEntry diaryEntry = DiaryEntry.createFromStart(startTime.get(i).getHourOfDay(),startTime.get(i).getMinuteOfHour(),Diary.INTERVAL_MINUTES);
				diaryEntry.setStation(stations.get(i));
				diaryEntry.setDevice(devices.get(i));
				diaryEntry.setPlace(places.get(i));
				diaryEntry.setDiaryId(diary.getId().intValue());
				diaryEntry.save();
			}
			
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = false;
			//Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, bmpFactoryOptions);
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));
		
			Toast.makeText(getBaseContext(), "Diary successfully added!", Toast.LENGTH_SHORT).show();
			Intent nextIntent = new Intent(getBaseContext(), MainActivity.class);
			nextIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(nextIntent);
			finish();
		}
	}
  
}
