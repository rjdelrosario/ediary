package com.orchestronix.ediary.activities;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("ValidFragment")
public class AfternoonTab extends ListFragment {
	
	private List<LocalTime> startTimeSegments;
	private List<LocalTime> endTimeSegments;
	private ArrayList<ListTableRow> customRow;
	private Context _context;
	
	public AfternoonTab(Context context) {
		this._context = context;
		
		Diary d = new Diary();
        d.createEntries();
        List<DiaryEntry> af = d.getAfEntries();
        
        this.startTimeSegments = new ArrayList<LocalTime>();
        this.endTimeSegments = new ArrayList<LocalTime>();
        this.customRow = new ArrayList<ListTableRow>();
        
        for(int i = 0; i < af.size(); i++) {
        	this.startTimeSegments.add(new LocalTime(af.get(i).startHour, af.get(i).startMinute));
        	this.endTimeSegments.add(new LocalTime(af.get(i).endHour, af.get(i).endMinute));
        	
        	final ListTableRow currentRow = new ListTableRow(this._context);
        	currentRow.setClickable(true);
        	currentRow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(currentRow.getStationSpinner().isEnabled()){
						currentRow.getStationSpinner().setEnabled(false);
						currentRow.getPlaceSpinner().setEnabled(false);
						currentRow.getDeviceSpinner().setEnabled(false);
						
						currentRow.setStation("-");
						currentRow.setPlace("-");
						currentRow.setDevice("-");
					}
					else {
						currentRow.getStationSpinner().setEnabled(true);
						currentRow.getPlaceSpinner().setEnabled(true);
						currentRow.getDeviceSpinner().setEnabled(true);
						
						if(getRows().indexOf(currentRow) > 0){
							ListTableRow row = getRows().get(getRows().indexOf(currentRow) - 1);
							currentRow.setStation(row.getStation());
							currentRow.setPlace(row.getPlace());
							currentRow.setDevice(row.getDevice());
						}
					}
				}
			});
        	
        	this.customRow.add(currentRow);
        }
        
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ListAdapter listAdapter = new ListAdapter(this._context, startTimeSegments, endTimeSegments, customRow);
		setListAdapter(listAdapter);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_fragment, null);
	}
	
	@Override
	public void onListItemClick(ListView list, View v, int position, long id) {
		
		Toast.makeText(getActivity(), getListView().getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
	}
	
	public List<LocalTime> getStartTimeSegments(){
		return this.startTimeSegments;
	}
	
	public List<LocalTime> getEndTimeSegments(){
		return this.endTimeSegments;
	}
	
	public List<String> getStations(){
		List<String> stations = new ArrayList<String>();
		
		for(int i=0; i<customRow.size(); i++){
			stations.add(customRow.get(i).getStation());
		}
		
		return stations;
	}
	
	public List<String> getDevices(){
		List<String> devices = new ArrayList<String>();
		
		for(int i=0; i<customRow.size(); i++){
			devices.add(customRow.get(i).getDevice());
		}
		
		return devices;
	}
	
	public List<String> getPlaces(){
		List<String> places = new ArrayList<String>();
		
		for(int i=0; i<customRow.size(); i++){
			places.add(customRow.get(i).getPlace());
		}
		
		return places;
	}
	
	public int getSize(){
		return customRow.size();
	}
	
	public List<ListTableRow> getRows(){
		return this.customRow;
	}
	
}