package com.orchestronix.ediary.activities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;
import com.orchestronix.ediary.models.DiaryEntry;

@SuppressLint("NewApi")
public class ExportActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_export);
		Button exportBtn = (Button) findViewById(R.id.btnExport);
		final DatePicker dtDate = (DatePicker) findViewById(R.id.dtDate);
		final Diary d = new Diary();
		d.createEntries();
		dtDate.setMaxDate(System.currentTimeMillis());
		
		exportBtn.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				File diaryFiles = new File(Environment.getExternalStorageDirectory(), "DiarySheets");
				File storage = new File(diaryFiles.getPath() + "/Exports");
				//final ArrayList<String> FilesInFolder = GetFiles(storage.getPath());
				String date = String.valueOf(dtDate.getMonth()+1) + "-"
						+ String.valueOf(dtDate.getDayOfMonth()) + "-"
						+ String.valueOf(dtDate.getYear());
				List<Diary> exportDiary = new ArrayList<Diary>();
				exportDiary = Diary.getEntries(date);
				if(exportDiary.size()!=0) {
				//	if(FilesInFolder.contains(date.toString() + ".csv")) {
				//		Toast.makeText(getBaseContext(), "File for this date is already exported", Toast.LENGTH_SHORT).show();
				//	}
				//	else{
						if(!storage.exists())	storage.mkdir();
						
						File fileName = new File(storage.getPath(), date+".csv");
						CSVWriter writer = null;
						try {
							writer = new CSVWriter(new FileWriter(fileName));
							
							for(int i = 0; i < exportDiary.size(); i++) {
								ArrayList<String> temp = new ArrayList<String>();
								
								temp.add(exportDiary.get(i).date);
								temp.add(exportDiary.get(i).area);
								temp.add(exportDiary.get(i).householdNo);
								List<DiaryEntry> entries = DiaryEntry.getDiary(exportDiary.get(i));
								
								for(int j = 0; j < entries.size(); j++) {
									DiaryEntry de = entries.get(j);
									LocalTime startTime = new LocalTime(de.getStartHour(), de.getStartMinute(), 0);
									LocalTime endTime = new LocalTime(de.getEndHour(), de.getEndMinute(), 0);
									temp.add(startTime.toString("h:mm")+" - "+endTime.toString("h:mm a"));
									temp.add(de.getStation());
									temp.add(de.getDevice());
									temp.add(de.getPlace());
								}
			
								exportDiary.get(i).exported();
								exportDiary.get(i).save();
								String[] temp1;
								temp1 = temp.toArray(new String[temp.size()]);
								writer.writeNext(temp1);
							}
							
							writer.close();
							Toast.makeText(getBaseContext(), "Entries exported", Toast.LENGTH_SHORT).show();
						
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					Intent intent = new Intent(ExportActivity.this, MainActivity.class);
					startActivity(intent);
					finish();
				//	}
				}
				else {
					Toast.makeText(getBaseContext(), "No entry to be exported", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	public List<String> createTimeEntries(List<DiaryEntry> de) {
		List<String> timeEntries = new ArrayList<String>();
		for(int i = 0; i < de.size(); i++) {
			String start = String.valueOf(de.get(i).startHour) + ":" + String.valueOf(de.get(i).startMinute);
			String end = String.valueOf(de.get(i).endHour) + ":" + String.valueOf(de.get(i).endMinute);
			timeEntries.add(start + "-" + end);
		}
		return timeEntries;
	}
	
	public ArrayList<String> GetFiles(String DirectoryPath) {
	    ArrayList<String> MyFiles = new ArrayList<String>();
	    File f = new File(DirectoryPath);

	    f.mkdirs();
	    File[] files = f.listFiles();
	    if (files.length == 0)
	        return null;
	    else {
	        for (int i=0; i<files.length; i++) 
	            MyFiles.add(files[i].getName());
	    }

	    return MyFiles;
	}
	
}
