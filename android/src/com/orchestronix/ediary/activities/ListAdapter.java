package com.orchestronix.ediary.activities;

import java.util.List;

import org.joda.time.LocalTime;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableLayout;

import com.orchestronix.ediary.R;

public class ListAdapter extends BaseAdapter {
	
	private Context _context;
	private List<LocalTime> _startTimeSegments;
	private List<LocalTime> _endTimeSegments;
	private List<ListTableRow> _customRow;
	
	public ListAdapter(Context context, List<LocalTime> startTimeSegments, List<LocalTime> endTimeSegments, List<ListTableRow> customRow){
		this._context = context;
		this._startTimeSegments = startTimeSegments;
		this._endTimeSegments = endTimeSegments;
		this._customRow = customRow;
	}
	
	@Override
	public int getCount() {
		return this._startTimeSegments.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }
		
		this._customRow.get(position).setStartTime(this._startTimeSegments.get(position));
		this._customRow.get(position).setEndTime(this._endTimeSegments.get(position));
		this._customRow.get(position).setTimeInterval();
		
		TableLayout tbLayout = (TableLayout) ((ViewGroup)convertView).getChildAt(0);
		((ViewGroup) tbLayout).removeView(tbLayout.getChildAt(0));
		//not so sure about this
		((ViewGroup) tbLayout).clearDisappearingChildren();
		tbLayout.addView(this._customRow.get(position));
		
		return convertView;
	}

}