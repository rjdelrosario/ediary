package com.orchestronix.ediary.activities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.orchestronix.ediary.R;
import com.orchestronix.ediary.models.Diary;

@SuppressLint("NewApi")
public class EditDiaryActivity extends Activity{
	Button btnNext;
	EditText txtArea, txtHousehold, txtPname, txtStatus;
	DatePicker dtDate;
	ArrayList<String> filterInfoId;
	ArrayList<String> filterInfo = new ArrayList<String>();
	Diary diary = new Diary();
	String id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_diary_information);
        ActiveAndroid.initialize(this);
		
		Intent intent = getIntent();
		filterInfoId = intent.getStringArrayListExtra("filterInfoId");
		
		if(filterInfoId.size()!=1) {
			for(int i = 0; i < filterInfoId.size()-1; i++)	filterInfo.add(filterInfoId.get(i));
			id = filterInfoId.get(filterInfoId.size()-1);
			diary = Diary.get(id); 
		}
		else {
			id = filterInfoId.get(0);
			diary = Diary.get(id);
		}
		

        btnNext = (Button) findViewById(R.id.editback);
        dtDate = (DatePicker) findViewById(R.id.date_editDate);
        txtArea = (EditText) findViewById(R.id.field_editArea);
        txtStatus = (EditText) findViewById(R.id.field_editStatus);
        txtHousehold = (EditText) findViewById(R.id.field_editHousehold);
        dtDate.setMaxDate(System.currentTimeMillis());
        
        String[] dates = diary.date.split("-");
        
        int month = Integer.parseInt(dates[0]);
        int day = Integer.parseInt(dates[1]);
        int year = Integer.parseInt(dates[2]);
        
        switch(diary.status) {
	     case 1:	txtStatus.setText("Saved"); break;
	     case 2:	txtStatus.setText("Exported"); break;
	     case 3:	txtStatus.setText("Sent"); break;
        }
        
        
        dtDate.updateDate(year, month-1, day);
        txtArea.setText(diary.area, BufferType.EDITABLE);
        txtHousehold.setText(diary.householdNo, BufferType.EDITABLE);
        txtStatus.setEnabled(false);
    	dtDate.setEnabled(false);
    	txtArea.setEnabled(false);
    	txtHousehold.setEnabled(false);
        btnNext.setText("Back");
    	
        btnNext.setOnClickListener( new OnClickListener() {
			public void onClick(View v){
				if(txtArea.getText().toString().trim().equals("") || txtHousehold.getText().toString().trim().equals("")){

					if(txtArea.getText().toString().trim().equals("")){
						txtArea.setError("Area is required!");
						txtArea.setHint("Enter area");
					}
					else if(txtHousehold.getText().toString().trim().equals("")){
						txtHousehold.setError("Household is required!");
						txtHousehold.setHint("Enter household");
					}
				}
				else {
					ArrayList<String> diaryInfo = new ArrayList<String>();
					diary.date = String.valueOf(dtDate.getMonth()+1) + "-" + String.valueOf(dtDate.getDayOfMonth() + "-"  + String.valueOf(dtDate.getYear()));
					diary.area = txtArea.getText().toString();
					diary.householdNo = txtHousehold.getText().toString();
					diary.save();
					
					diaryInfo.add(diary.date);
					diaryInfo.add(diary.area);
					diaryInfo.add(diary.householdNo);
					diaryInfo.add(id);
					
					
					if(filterInfo.size()!=0)	diaryInfo.addAll(filterInfo);
					if(btnNext.getText()=="Back") {
						/*Intent i = new Intent(getBaseContext(), EditTimelineActivity.class);
						i.putStringArrayListExtra("diaryInfo", diaryInfo);
						startActivity(i);*/
						Intent i = new Intent(getBaseContext(), MainActivity.class);
						if(filterInfoId.size()!=1)	i.putStringArrayListExtra("filterInfo", filterInfo);
						startActivity(i);
					}
					else {
						dtDate.setEnabled(false);
		            	txtArea.setEnabled(false);
		            	txtHousehold.setEnabled(false);
		            	btnNext.setText("Back");
					}
					/*
					Toast.makeText(getBaseContext(), "Entry has been saved and edited", Toast.LENGTH_SHORT).show();
					Intent nextIntent = new Intent(getBaseContext(), MainActivity.class);
					startActivity(nextIntent);
					*/
				}
			}
		});
	}
	
	 	@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu items for use in the action bar
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.edit, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle presses on the action bar items
	        switch (item.getItemId()) {
	            case R.id.add_action_edit:
	            	if(diary.status == 2) {
	            		Toast.makeText(getBaseContext(), "Exported entries cannot be edited!", Toast.LENGTH_SHORT).show();
	            	}
	            	else if(diary.status == 3) {
	            		Toast.makeText(getBaseContext(), "Sent entries cannot be edited!", Toast.LENGTH_SHORT).show();
	            	}
	            	else {
		            	dtDate.setEnabled(true);
		            	txtArea.setEnabled(true);
		            	txtHousehold.setEnabled(true);
		            	btnNext.setText("Save");
		            	btnNext.setEnabled(true);
	            	}
	                
	        }
	        return true;
	    }
}
