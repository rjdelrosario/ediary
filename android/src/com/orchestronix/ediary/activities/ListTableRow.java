package com.orchestronix.ediary.activities;

import org.joda.time.LocalTime;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.orchestronix.ediary.R;

public class ListTableRow extends TableRow {
	
	private Context _context;
	private TextView txtTimeInterval;
	private Spinner addStation, addDevice, addPlace;
	private LocalTime startTime, endTime;
	
	public ListTableRow(Context context) {
		super(context);
		this._context = context;
		
		this.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
		
		this.txtTimeInterval = new TextView(this._context);
		//this.txtTimeInterval.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		this.txtTimeInterval.setLayoutParams(new TableRow.LayoutParams(250, LayoutParams.WRAP_CONTENT, 0.25f));
		
		((TableRow.LayoutParams) this.txtTimeInterval.getLayoutParams()).setMargins(0,15,15,15);
		
	    ArrayAdapter<CharSequence> dataAdapter;
	    TableRow.LayoutParams trLayout = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 0.25f);
	    
	    trLayout.setMargins(0,15,15,15);
	    
	    this.addStation = new Spinner(this._context);
	    this.addStation.setLayoutParams(trLayout);
        dataAdapter = ArrayAdapter.createFromResource(this._context, R.array.stationsArray, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        this.addStation.setAdapter(dataAdapter);
        this.addStation.setEnabled(false);
	    
        this.addPlace = new Spinner(this._context);
        this.addPlace.setLayoutParams(trLayout);
        dataAdapter = ArrayAdapter.createFromResource(_context, R.array.placesArray, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        this.addPlace.setAdapter(dataAdapter);
        this.addPlace.setEnabled(false);
        
        this.addDevice = new Spinner(this._context);
        this.addDevice.setLayoutParams(trLayout);
        dataAdapter = ArrayAdapter.createFromResource(_context, R.array.devicesArray, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        this.addDevice.setAdapter(dataAdapter);
        this.addDevice.setEnabled(false);
        
		super.addView(this.txtTimeInterval);
		super.addView(this.addStation);
		super.addView(this.addPlace);
		super.addView(this.addDevice);
	}
	
	public Spinner getStationSpinner(){
		return this.addStation;
	}
	
	public Spinner getPlaceSpinner(){
		return this.addPlace;
	}
	
	public Spinner getDeviceSpinner(){
		return this.addDevice;
	}
	
	public LocalTime getStartTime(){
		return this.startTime;
	}
	
	public LocalTime getEndTime(){
		return this.endTime;
	}
	
	public String getStation(){
		return this.addStation.getSelectedItem().toString();
	}
	
	public String getDevice(){
		return this.addDevice.getSelectedItem().toString();
	}
	
	public String getPlace(){
		return this.addPlace.getSelectedItem().toString();
	}
	
	public void setStartTime(LocalTime startTime){
		this.startTime = startTime;
	}
	
	public void setEndTime(LocalTime endTime){
		this.endTime = endTime;
	}
	
	public void setStation(String station){
		@SuppressWarnings("unchecked")
		ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>) this.addStation.getAdapter();
		this.addStation.setSelection(adapter.getPosition(station));
	}
	
	public void setDevice(String device){
		@SuppressWarnings("unchecked")
		ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>) this.addDevice.getAdapter();
		this.addDevice.setSelection(adapter.getPosition(device));
	}
	
	public void setPlace(String place){
		@SuppressWarnings("unchecked")
		ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>) this.addPlace.getAdapter();
		this.addPlace.setSelection(adapter.getPosition(place));
	}
	
	public void setTimeInterval(){
		this.txtTimeInterval.setText(this.startTime.toString("h:mm")+" - "+this.endTime.toString("h:mm a"));
	}
	
}
