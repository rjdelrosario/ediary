package com.orchestronix.ediary;

import roboguice.RoboGuice;
import android.app.Application;

public class App extends Application {
	public static final String TAG = "App";

	@Override
	public void onCreate() {
		super.onCreate();
		init();
	}

	public void init() {
		RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
				RoboGuice.newDefaultRoboModule(this), new GuiceModule());
	}
}
