package com.orchestronix.ediary;

import com.google.inject.*;
import com.orchestronix.ediary.services.*;

public class GuiceModule implements Module {
	@Override
	public void configure(Binder b) {
		b.bind(DiaryService.class).to(DiaryServiceImpl.class).in(Singleton.class);
	}
}
